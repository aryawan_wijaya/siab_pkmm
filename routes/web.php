<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/','MasterController@index');

Route::get('/mitigasi/create','mitigasiController@vcreate')->middleware('auth');
Route::get('/mitigasi','mitigasiController@index')->middleware('auth');
Route::get('/mitigasi/showData/{id}','mitigasiController@showData')->middleware('auth');

Route::get('/status','statusController@index')->middleware('auth');
Route::get('/status/create','statusController@vcreate')->middleware('auth');
// Route::get('/siab','MasterController@index');


Auth::routes();
 
Route::get('/Dasboard', 'HomeController@index')->name('Dasboard')->middleware('auth');


Route::get('/siab', function(){
    return view('auth.login');
    });

Route::get('/siab/daftarAdmin', function(){
    return view('auth.register');
    });