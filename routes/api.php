<?php

use Illuminate\Http\Request;
use App\Mitigasi;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Route::post('/mitigasi/create','mitigasiController@create');
// Route::post('/mitigasi/create', function (Request $req){
//     return Mitigasi::create($req->all);
// });
Route::get('/mitigasi', 'mitigasiController@allData');
Route::post('/mitigasi/create', 'mitigasiController@create');
Route::get('/mitigasi/del/{id}','mitigasiController@delete');
Route::get('/mitigasi/{id}','mitigasiController@show');
Route::post('/mitigasi/edit','mitigasiController@edit');

Route::get('/status','statusController@showAll');
Route::post('/status/create','statusController@create');
Route::post('/status/update','statusController@edit');

Route::get('/lokasi','titikController@allLokasi');

Route::get('/berita','twitterApiController@getTimeLineTwitter');
// Route::get('/berita', function()
// {
//     return Twitter::getUserTimeline(array('screen_name' => 'BPPTKG', 'count' => 20, 'format' => 'json'));
// });
