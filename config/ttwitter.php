<?php

// You can find the keys here : https://apps.twitter.com/

return [
	'debug'               => function_exists('env') ? env('APP_DEBUG', false) : false,

	'API_URL'             => 'api.twitter.com',
	'UPLOAD_URL'          => 'upload.twitter.com',
	'API_VERSION'         => '1.1',
	'AUTHENTICATE_URL'    => 'https://api.twitter.com/oauth/authenticate',
	'AUTHORIZE_URL'       => 'https://api.twitter.com/oauth/authorize',
	'ACCESS_TOKEN_URL'    => 'https://api.twitter.com/oauth/access_token',
	'REQUEST_TOKEN_URL'   => 'https://api.twitter.com/oauth/request_token',
	'USE_SSL'             => true,

	'CONSUMER_KEY'        => function_exists('env') ? env('TWITTER_CONSUMER_KEY', '') : '',
	'CONSUMER_SECRET'     => function_exists('env') ? env('TWITTER_CONSUMER_SECRET', '') : '',
	'ACCESS_TOKEN'        => function_exists('env') ? env('TWITTER_ACCESS_TOKEN', '') : '',
	'ACCESS_TOKEN_SECRET' => function_exists('env') ? env('TWITTER_ACCESS_TOKEN_SECRET', '') : '',

	// 'CONSUMER_KEY'        => 'JbT6ubQKUsvoJ9J8a5DhrplFt',
	// 'CONSUMER_SECRET'     => 'uAYCxKwbN56ValpOe993ZuO02ojvM4gSovXte5aXTOpBKnR1U4',
	// 'ACCESS_TOKEN'        => '126642014-S6mJAaDc6YTssYaGN8MWKvZMkxYtfoN5itLhMVwE',
	// 'ACCESS_TOKEN_SECRET' => 'eClNnAMfBOsiQZ6D9uwZJu2rWOsnHPgnero3kwjDAKesa',
];
 