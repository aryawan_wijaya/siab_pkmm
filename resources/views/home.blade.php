@extends('Layout.Layout')

@section('judul','SIAB Merapi-Mitigasi')


@section('content')
<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="text-right" >
            <a class="btn btn-info " href="{{ route('logout') }}"
                onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">
                {{ __('Logout') }}
            </a>

        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
        </form>
        </div>
    </div>
</div>


<br>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                    </div>
                        <div class="btn-group" role="group" aria-label="Basic example">
                        <a href="/mitigasi" class="btn btn-secondary" role="button"> Mitigasi </a>
                        <a href="/status" class="btn btn-danger" role="button"> Status </a>
                        </div>
                </div>
                
            </div>
        </div>
    </div>
</div>
@endsection
 