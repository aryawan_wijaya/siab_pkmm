@extends('Layout.Layout')

@section('judul','SIAB Merapi-Mitigasi')


@section('content')

<script>
   $(document).ready(function(){
        $.get("http://localhost:8000/api/status",
         function(data,status){
            // alert(data.status);
            var output = "<table class='table table-striped table-dark' id='dataTable' width='100%' cellspacing='0'>"+
            "<thead class='table table-dark text-center'><tr><td> STATUS MERAPI SAAT INI</td></tr></thead>"+
            "<tbody class='table table-dark'><tr><td class='text-center'>"+ data.status+"</td></tr></tbody></table>";        
            $("#tabStatus2").html(output);
         }); 
      });
</script>

<script>
$(document).ready(function(){
   $("#TombolEdit").click(function(){
      //alert("Status Berhasil Diubah");
      $.post("http://localhost:8000/api/status/update",
      {
         // id : 1,
         status : document.getElementById("statusUbah").value,
      },
      function(data,Status){
         alert("Status Berhasil Diubah");
      });
   });
});
</script>

<div class="mx-auto" style="width: 70%;">
   <div class="text-white text-center">
       <h1>Status</h1>
   </div>
   

<div class="col text-left">
            <a href="/Dasboard" class="btn btn-light">Home</a>
</div>

      <br>
      <div id="tabStatus2">
        <table class='table table-striped table-dark' id='dataTable' width='100%' cellspacing='0'>
         <thead class='table table-dark text-center'>
            <tr>
               <th>Status</th>
            </tr>
         </thead>
         <tbody>
            <tr>
               <td></td>
            </tr>
         </tbody>
         </table>
      </div>
      <br>
      <br>
      <br>
      <form id="editStatus">
      <h2 class="text-white font-weight-bold text-center">Form Untuk Ubah Status Merapi</h2>
      <select class="form-control" id="statusUbah">
        <option value="Normal">Normal</option>
        <option value="Waspada">Waspada</option>
        <option value="Siaga">Siaga</option>
        <option value="Awas">Awas</option>
      </select>
      </br>
      <button id="TombolEdit" class="btn btn-light">Edit</button>
      </form>
</div>


@endsection()