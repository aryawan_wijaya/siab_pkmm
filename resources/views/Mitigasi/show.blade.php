@extends('Layout.Layout')

@section('judul','SIAB Merapi-Mitigasi')

@section('content')
<script>
$(document).ready(function(){
      $("#SubmitEdit").click(function(){
         // alert("Berhasil");
         //$.post('url',dataarray,function);
         //$.get('url/param',function);
         $.post("http://localhost:8000/api/mitigasi/edit",
         {
            id : document.getElementById("mitigasiId").value,
            judul : document.getElementById("judul").value,
            isi   : document.getElementById("isi").value
         },
         function(data,status){
            alert("Berhasil");
         }); 
      });
   });
</script>



<div class="mx-auto" style="width: 70%;">
<div class="text-white text-center">
    <h1>Mitigasi</h1>
</div>
<a href="/mitigasi" class="btn btn-light">Lihat Mitigiasi Lengkap</a>
    <form id="createMitigasi">
      <div class="form-group">
      <br>
         <input type="hidden" value ="{{$mitigasi->id}}" id="mitigasiId">
        <label for="judul" class="text-white">Judul</label>
        <input type="text" id="judul" class="form-control" value="{{$mitigasi->judul}}"><br>
        <label for="isi" class="text-white">Isi</label>
        <textarea class="form-control" rows="10" id="isi">{{$mitigasi->isi}}</textarea>
        <br><br>
        <div class="col text-center">
            <button id="SubmitEdit" class="btn btn-light">Submit</button>
            <button type="reset" class="btn btn-light">Reset</button>
         </div>
      </div>
    </form>
   





</div>


@endsection()