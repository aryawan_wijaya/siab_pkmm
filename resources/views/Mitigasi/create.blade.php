<!-- <!DOCTYPE html>
<html lang="en">
<head>
    <title>Create Mitigasi</title>
    
</head> -->

@extends('Layout.Layout')

@section('judul','SIAB Merapi-create mitigasi')


@section('content')
<script>
   $(document).ready(function(){
      $("#ajaxSubmit").click(function(){
         //$.post('url',dataarray,function);
         //$.get('url/param',function);
         $.post("http://localhost:8000/api/mitigasi/create",
         {
            judul : document.getElementById("judul").value,
            isi   : document.getElementById("isi").value
         },
         function(data,status){
            alert("Berhasil");
         }); 
      });
   });
</script>
<div class="mx-auto" style="width: 70%;">
<div class="text-white">
    <h1>Create Mitigasi</h1>
</div>
   <a href="/mitigasi" class="btn btn-light">Lihat Mitigiasi</a>
    <form id="createMitigasi">
      <div class="form-group">
      <br>
        <label for="judul" class="text-white">Judul</label>
        <input type="text" id="judul" class="form-control"><br>
        <label for="isi" class="text-white">Isi</label>
        <textarea class="form-control" rows="10" id="isi"></textarea>
        <br><br>
        <div class="col text-center">
            <button id="ajaxSubmit" class="btn btn-light">Submit</button>
            <button type="reset" class="btn btn-light">Reset</button>
         </div>
      </div>
    </form>
    
</div>
@endsection()
   
