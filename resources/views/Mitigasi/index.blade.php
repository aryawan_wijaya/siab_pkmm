@extends('Layout.Layout')

@section('judul','SIAB Merapi-Mitigasi')


@section('content')
<script>
   $(document).ready(function(){
         $.get("http://localhost:8000/api/mitigasi",
         function(data,status){
            //  alert(data.mitigasi[0].judul);
            var output = "<table class='table table-striped table-dark' id='dataTable' width='100%' " + 
            "cellspacing='0'><thead class='table table-dark text-center'><tr><th>Judul</th><th>ISI</th><th>Aksi</th></tr></thead><tbody>";
					for (var i in data.mitigasi) {
						output += "<tr><td>" +
						data.mitigasi[i].judul +
						"</td><td>" +
						data.mitigasi[i].isi +
						"</td><td>" +
						'<a href="api/mitigasi/del/'+ data.mitigasi[i].id +'">Delete</a>'+
						"</td><td>"+
                        '<a href="/mitigasi/showData/'+ data.mitigasi[i].id +'">Edit</a>'+
                        "</td></tr>";
					};
					output += "</tbody></table>";
            $("#tabMitigasi").html(output);
         }); 
      });
</script>

 
<div class="mx-auto" style="width: 70%;">
<div class="text-white text-center">
    <h1>Mitigasi</h1>
</div>

<div class="col text-left">
            <a href="/mitigasi/create" class="btn btn-light">New Mitigasi</a>
            <a href="/Dasboard" class="btn btn-light">Home</a>
</div>






<br>
   <div id="tabMitigasi">
        <table class="table table-striped" id="dataTable" width="100%" cellspacing="0">
        <thead class="table table-dark text-center">
            <td> Judul </td>
            <td> Mitigasi </td>
            
            <td> </td>
            </thead>
            <tbody>
            </tbody>
        </table>
   </div>
</div>
@endsection()