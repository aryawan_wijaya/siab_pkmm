@extends('Layout.Layout')

@section('judul','Aplikasi - SIAB Merapi')


@section('content')
<div class="container h-100">
      <div class="row h-100 align-items-center justify-content-center text-center">
        <div class="col-lg-10 align-self-end">
          <img src="img/4.png" class="logo">
          <h1 class="text-uppercase text-white font-weight-bold">Sistem Informasi Antisipasi Bencana Merapi</h1>
          <hr class="divider my-4">
        </div>
        <div class="col-lg-8 align-self-baseline">
          <a href="www.playstore.com" target="_blank"><img src="img/3.png" class="playstore"></a>
        </div>
      </div>
    </div>
@endsection()

