<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mitigasi extends Model
{
    protected $fillable = [
        'judul',
        'isi'
    ];
}
