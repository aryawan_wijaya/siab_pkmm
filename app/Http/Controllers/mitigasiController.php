<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mitigasi;
class mitigasiController extends Controller
{
    //return web
    public function vcreate()
    {
        return view('Mitigasi.create');
    }

    public function index()
    { 
        return view('Mitigasi.index');
    }

    public function showData($id)
    {
        $mitigasi = Mitigasi::Find($id);
        return view('Mitigasi.show',compact('mitigasi'));
    }


    //return json
    public function create(Request $req)
    {
        $mitigasi = new Mitigasi;
        $mitigasi->judul = $req->input('judul');
        $mitigasi->isi = $req->input('isi');
        $mitigasi->save();
        // return "Data berhasil";
        return Response()->json([
            'status' => "Data Created",
            'judul' => $mitigasi->judul , 
            'isi' => $mitigasi->isi 
        ], 200);
        // return Response::json(['judul' => $mitigasi->judul , 'isi' => $mitigasi->isi ]);
    }

    

    public function allData()
    {
        $mitigasi = Mitigasi::all();
        return Response()->json(compact('mitigasi'));   
    }

    public function delete($id)
    {
        $mitigasi =Mitigasi::Find($id);
        $mitigasi -> delete();
        return redirect('/mitigasi');
    }

    public function show($id)
    {
        $mitigasi =Mitigasi::Find($id);
        return Response()->json([
            'judul' =>$mitigasi->judul,
            'isi' =>$mitigasi->isi
        ], 200);
    }

    public function edit(Request $req)
    {
        $id = $req->input('id');
        $mitigasi= Mitigasi::Find($id);
        $mitigasi->judul = $req->input('judul');
        $mitigasi->isi = $req->input('isi');
        $mitigasi->save();
        return Response()->json([
            'status' =>"Data updated",
            'judul' =>$mitigasi->judul,
            'isi' =>$mitigasi->isi
        ], 200);
    }
}
