<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Status;
class statusController extends Controller
{
    //web
    public function vcreate()
    {
        return view('Status.create');
    }

    public function index()
    {
        return view('Status.show');
    }



    //api
    public function create(Request $req)
    {
          $status = new Status;
          $status->status = $req->input('status');
          $status->save();
          return response()->json([
              'Status' =>"Data Created",
              'status' =>$status->status
            ], 200);
    }

    public function edit(Request $req)
    {
        // $id  =$req->input('id');
        $status = Status::find(1);
        $status->status = $req->input('status');
        $status->save();
        return response()->json([
            'Status' =>"Data updated",
            'status' =>$status->status
          ], 200);
    }

    public function showAll()
    {
        // $status = Status::all();
        // return response()->json(compact('status'));
        $status = Status::find(1);
        // print_r($status);exit;
        return response()->json([
            'status' =>$status->status
          ], 200);
    }
}
